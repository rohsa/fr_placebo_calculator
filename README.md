# Predict Tonality/Technical From Frequency Response

## Notice

**Don't take it seriously, these models doesn't have human placebo.**

Pre-trained models are saved in models folder, you can use `predict.py` to predict your frequency response file, check the `Usage` section below.

If you want to train the model youself, you will need the dataset which is not public. You can ask me for the dataset, but you will need premissions from following reviewers:

- Banbeucmas
- Crinacle
- Precog
- MRS (Super Review)
- TechPowerUp

## Usage

For now it only works with IEM measurement.

Initialize:

``` text
python3 -m venv ~/.venv_fr_placebo_calculator
. ~/.venv_fr_placebo_calculator/bin/activate
pip3 install -r requirements.txt
```

Run the prediction for your frequency response file:

``` text
. ~/.venv_fr_placebo_calculator/bin/activate
python3 predict.py "Your Measurement L.txt"
```

If you want to try other model you can edit `predict.py`, for example use decision tree:

``` text
m = model.DecisionTreeModel()
# m = model.SimpleNeuralNetworkModel()
```

## Accuracy

Errors (Mean Absolute Error) are calculated from test set (30% of total data).

Predicted values of each rank:

- S+: 1.0
- S: 0.875
- S-: 0.75
- A+: 0.625
- A: 0.5
- A-: 0.375
- B+: 0.25
- B: 0.125
- B-: 0.0
- C+: -0.125
- C: -0.25
- C-: -0.375
- D+: -0.5
- D: -0.625
- D-: -0.75
- E: -0.875
- F: -1.0

For example, if actual rank of an iem is C and predicted result is C+, then error of this iem is 0.125, MAE is the mean aboslute error of all iems in the test set.

DecisionTreeModel:

``` text
tonality error: 0.22343096234309623
technicality error: 0.22709205020920503
```

SimpleNeuralNetworkModel:

``` text
38/38 [==============================] - 0s 1ms/step - loss: 0.0426 - mae: 0.1329
38/38 [==============================] - 0s 1ms/step - loss: 0.0419 - mae: 0.1348
```

RecurrentNeuralNetworkModel:

``` text
38/38 [==============================] - 0s 7ms/step - loss: 0.0570 - mae: 0.1701
38/38 [==============================] - 0s 7ms/step - loss: 0.0585 - mae: 0.1657
```

## License

The MIT License.

© 2022 Rohsa All rights reserved. 


import os
import json
import hashlib
import numpy as np
import model

RankingPath = "../fr_database/Crinacle IEMs Ranking"
DatabasePaths = [
    "../fr_database/Banbeu IEMs",
    "../fr_database/Crinacle IEMs",
    "../fr_database/Precog IEMs",
    "../fr_database/SuperReview IEMs",
    "../fr_database/TechPowerUp IEMs"
]
NameReaplcements = [
    ("tin hifi", "tin"),
    ("tin audio", "tin"),
    ("64 audio", "64audio"),
    ("a/u12t", "a12t/u12t"),
    ("a/u18 tzar", "a18s/u18tzar"),
    ("viento\n", "vientob"),
    ("ue18+ (2nd gen)", "ue18+ pro gen 2"),
    ("ue18+ (3rd gen)", "ue18+ pro gen 3"),
    ("justear xje-mh2", "justear"),
    ("see audio", "seeaudio"),
    ("massdrop", "drop"),
    ("x hbb", ""),
    ("x aaw", ""),
    ("-", ""),
    ("\n", ""),
    ("\"", ""),
]
FilenameBlacklist = ["filterless", "no filter", "filter removed"]

def normalize_index_name(name):
    name = name.lower()
    for a, b in NameReaplcements:
        name = name.replace(a, b)
    name = name.split("(")[0].split("/")[0]
    return name

def normalize_ranking_name(name):
    name = name.lower()
    for a, b in NameReaplcements:
        name = name.replace(a, b)
    name = name.split("(")[0]
    mixed_names = name.split("/")
    if len(mixed_names) == 1:
        return (name,)
    brand = name.split()[0]
    return [mixed_names[0]] + [f"{brand} {n}" for n in mixed_names[1:]]

def build_dataset(ranking_path, database_paths):
    ranking = json.load(open(os.path.join(ranking_path, "ranking.json"), "r"))
    index = [
        (normalize_index_name(k).split(), os.path.join(database_path, filename))
        for database_path in database_paths
        for k, v in json.load(open(
            os.path.join(database_path, "index.json"), "r")).items()
        for filename in v
        if not any(1 for w in FilenameBlacklist if w in filename.lower())
    ]
    entries = []
    not_found_names = []
    for name, (tonality, technicality) in ranking.items():
        found = False
        for name in normalize_ranking_name(name):
            name_parts = name.split()
            for k, v in index:
                if all(p in k for p in name_parts):
                    entries.append((v, tonality, technicality))
                    found = True
        if not found:
            not_found_names.append(name)
    print(f"found {len(entries)} data sample from "
        f"{len(ranking)-len(not_found_names)}/{len(ranking)} models from ranking")
    xs = []
    ys = []
    for path, tonality, technicality in entries:
        x = model.load_fr_as_data(path)
        if x is None:
            print(f"warning: file {path} not found")
            continue
        y = (model.RanksToValue[tonality], model.RanksToValue[technicality])
        xs.append(x)
        ys.append(y)
    xs = np.array(xs)
    ys = np.array(ys)
    return xs, ys

def get_model_options(ranking_path, database_paths):
    return (f"OctaveBands: {len(model.OctaveBands)}\n"
        f"NormalizeHz: {model.NormalizeHz}\n"
        f"NormalizeScale: {model.NormalizeScale}\n"
        f"UseRelativeSPL: {model.UseRelativeSPL}\n"
        f"RankingPath: {ranking_path}\n"
        f"DatabasePath: {','.join(database_paths)}\n")

def load_dataset(ranking_path, database_paths):
    options = get_model_options(ranking_path, database_paths)
    if (os.path.isfile("datasets/options.txt") and
        os.path.isfile("datasets/xs.npy") and
        os.path.isfile("datasets/ys.npy") and
        open("datasets/options.txt", "r").read() == options):
        print("loading dataset from datasets folder, if you have original measurement data and want to generate new dataset please delete npy files under datasets folder")
        with open("datasets/xs.npy", "rb") as f:
            xs = np.load(f)
        with open("datasets/ys.npy", "rb") as f:
            ys = np.load(f)
    else:
        xs, ys = build_dataset(ranking_path, database_paths)
        with open("datasets/xs.npy", "wb") as f:
            np.save(f, xs)
        with open("datasets/ys.npy", "wb") as f:
            np.save(f, ys)
        with open("datasets/options.txt", "w") as f:
            f.write(options)
    print(f"loaded {xs.shape[0]} samples")
    return xs, ys

def main():
    xs, ys = load_dataset(RankingPath, DatabasePaths)
    # m = model.DecisionTreeModel()
    m = model.SimpleNeuralNetworkModel()
    # m = model.RecurrentNeuralNetworkModel()
    m.train(xs, ys)

if __name__ == "__main__":
    main()


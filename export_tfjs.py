"""
Export models for tensorflow js.
"""
import tensorflowjs as tfjs
import model

def export(m):
    m.predict([[0.0]*len(model.OctaveBands)])
    tfjs.converters.save_keras_model(
        m.tonality_model, m.tonality_model_path.replace(".ckpt", ".tfjs"))
    tfjs.converters.save_keras_model(
        m.technicality_model, m.technicality_model_path.replace(".ckpt", ".tfjs"))

def main():
    export(model.SimpleNeuralNetworkModel())
    export(model.RecurrentNeuralNetworkModel())

if __name__ == "__main__":
    main()


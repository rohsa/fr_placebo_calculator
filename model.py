import os
import re
import math
import numpy as np

def octave_bands(fraction):
    """Generate 1/fraction octave bands"""
    bands = [FrequenceRange[0]]
    steps = 2 ** (1/fraction)
    while bands[-1] < FrequenceRange[1]:
        value = round(bands[-1] * steps * 10) / 10
        if value >= 1000:
            value = int(value) // 10 * 10
        elif value >= 100:
            value = int(value)
        bands.append(value)
    bands[-1] = FrequenceRange[1]
    return bands

FrequenceRange = (20, 20000)
SplitFRRowRegex = re.compile("[\s,;]+")
OctaveBands = octave_bands(6)
NormalizeHz = 500
NormalizeIndex = next(i for i, o in enumerate(OctaveBands) if o >= NormalizeHz)
NormalizeScale = 0.1
Ranks = [rr for r in "FEDCBAS" for rr in ([r] if r in "FE" else [r+"-", r, r+"+"])]
RanksToValue = { r: i / (len(Ranks)-1) * 2 - 1 for i, r in enumerate(Ranks) }
ValueToRanks = sorted(RanksToValue.items(), key = lambda p: -p[1])
UseRelativeSPL = True

def load_fr(path):
    """
    Load frequency response from file path.
    Will resample to `OctaveBands` and normalize to `NormalizeHz` at 0db.
    """
    xs = []
    ys = []
    # the measurement data is NOT csv, we can't use pandas here
    # it contains junks like "source    Headset Mic 1 Low Range"
    with open(path, "r", errors="replace") as f:
        for line in f:
            if not line or line[0] == "*":
                continue
            cells = SplitFRRowRegex.split(line)
            if len(cells) >= 2:
                try:
                    x = float(cells[0]) # Frequence
                    y = float(cells[1]) # SPL
                    if not (math.isnan(x) or math.isnan(y)):
                        xs.append(x)
                        ys.append(y)
                except ValueError:
                    continue
    if not xs:
        raise RuntimeError(f"Load file error: {path}")
    np_xs = np.array(xs)
    np_ys = np.array(ys)
    # resample
    sample = np.interp(OctaveBands, np_xs, np_ys)
    # normalize
    sample -= sample[NormalizeIndex]
    sample *= NormalizeScale
    return sample

def to_relative(fr):
    """Convert spl values to relative values (delta to previous band)"""
    v = fr[0]
    g = []
    for f in fr:
        g.append(f - v)
        v = f
    return g

_convert_fr_data = to_relative if UseRelativeSPL else (lambda fr: fr)
def load_fr_as_data(path):
    """Load frequency response as data for prediction"""
    if os.path.isfile(path) and os.path.getsize(path) > 0:
        return _convert_fr_data(load_fr(path))
    return None

def get_rank_name(score):
    """Convert score to rank name"""
    for i, (name, x) in enumerate(ValueToRanks):
        if score >= x:
            if i > 0:
                prev_name, prev_x = ValueToRanks[i-1]
                if prev_x - score < score - x:
                    return prev_name
            return name
    return ValueToRanks[-1][0]

class DecisionTreeModel(object):
    """
    Predict tonality and technicality by decision tree.
    To get best result:
    - Set OctaveBands to 1/6
    - Set UseRelativeSPL to True
    """

    def train(self, xs, ys):
        import pydotplus
        import pickle
        from sklearn.tree import DecisionTreeRegressor
        from sklearn.model_selection import train_test_split
        from sklearn import metrics
        from sklearn.tree import export_graphviz
        from io import StringIO

        x_train, x_test, y_train, y_test = train_test_split(
            xs, ys, test_size=0.3, random_state=1)
        tonality_model = DecisionTreeRegressor(random_state=1)
        tonality_model.fit(x_train, y_train[:,0])
        technicality_model = DecisionTreeRegressor(random_state=1)
        technicality_model.fit(x_train, y_train[:,1])
        y0_pred = tonality_model.predict(x_test)
        y1_pred = technicality_model.predict(x_test)
        print("tonality error:", np.abs(y0_pred - y_test[:,0]).mean())
        print("technicality error:", np.abs(y1_pred - y_test[:,1]).mean())

        with open("models/tonality_dicision_tree.pkl", "wb") as f:
            pickle.dump(tonality_model, f)
        with open("models/technicality_dicision_tree.pkl", "wb") as f:
            pickle.dump(technicality_model, f)

        tonality_dot_data = StringIO()
        technicality_dot_data = StringIO()
        export_graphviz(tonality_model, out_file=tonality_dot_data,
            filled=True, rounded=True, special_characters=True,
            feature_names=[f"{f}Hz" for f in OctaveBands])
        export_graphviz(technicality_model, out_file=technicality_dot_data,
            filled=True, rounded=True, special_characters=True,
            feature_names=[f"{f}Hz" for f in OctaveBands])
        tonality_graph = pydotplus.graph_from_dot_data(tonality_dot_data.getvalue())
        technicality_graph = pydotplus.graph_from_dot_data(technicality_dot_data.getvalue())
        tonality_graph.write_svg("models/tonality_dicision_tree.svg")
        technicality_graph.write_svg("models/technicality_dicision_tree.svg")

    def predict(self, xs):
        import pickle

        with open("models/tonality_dicision_tree.pkl", "rb") as f:
            tonality_model = pickle.load(f)
        with open("models/technicality_dicision_tree.pkl", "rb") as f:
            technicality_model = pickle.load(f)
        y0_pred = tonality_model.predict(xs)
        y1_pred = technicality_model.predict(xs)
        return np.array(list(zip(y0_pred, y1_pred)))

class _TensorflowModelBase(object):
    """Base class of tensorflow models"""

    def __init__(self):
        self.tonality_model = None
        self.technicality_model = None
        self.tonality_model_path = None
        self.technicality_model_path = None
        self.epochs = 1000
        self.optimizer = "adam"
        self.loss = "mse"
        self.save_best_only = False

    def train(self, xs, ys):
        import tensorflow as tf
        from sklearn.model_selection import train_test_split

        x_train, x_test, y_train, y_test = train_test_split(
            xs, ys, test_size=0.3, random_state=1)
        tonality_callback = tf.keras.callbacks.ModelCheckpoint(
            self.tonality_model_path, save_weights_only=True,
            save_best_only=self.save_best_only, monitor="val_mae")
        technicality_callback = tf.keras.callbacks.ModelCheckpoint(
            self.technicality_model_path, save_weights_only=True,
            save_best_only=self.save_best_only, monitor="val_mae")
        self.tonality_model.compile(
            optimizer=self.optimizer, loss=self.loss, metrics=["mae"])
        self.technicality_model.compile(
            optimizer=self.optimizer, loss=self.loss, metrics=["mae"])

        print("train:")
        self.tonality_model.fit(x_train, y_train[:,0],
            epochs=self.epochs, callbacks=[tonality_callback],
            validation_data=(x_test, y_test[:,0]))
        self.technicality_model.fit(x_train, y_train[:,1],
            epochs=self.epochs, callbacks=[technicality_callback],
            validation_data=(x_test, y_test[:,1]))

        # Same as validation data, just for README
        print("evalulate:")
        self.tonality_model.evaluate(x_test, y_test[:,0])
        self.technicality_model.evaluate(x_test, y_test[:,1])

    def predict(self, xs):
        self.tonality_model.load_weights(self.tonality_model_path)
        self.technicality_model.load_weights(self.technicality_model_path)
        y0_pred = self.tonality_model.predict(xs)
        y1_pred = self.technicality_model.predict(xs)
        return np.array(list(zip(y0_pred, y1_pred)))

class SimpleNeuralNetworkModel(_TensorflowModelBase):
    """
    Predict tonality and technicality by simple neural network (multi dense layer).
    To get best result:
    - Set OctaveBands to 1/6
    - Set UseRelativeSPL to True
    """
    Version = 1.1

    def __init__(self):
        super().__init__()
        import tensorflow as tf
        self.tonality_model = tf.keras.models.Sequential([
          tf.keras.layers.Dense(128, activation="relu", input_shape=[len(OctaveBands)]),
          tf.keras.layers.Dropout(0.2),
          tf.keras.layers.Dense(64, activation="relu"),
          tf.keras.layers.Dropout(0.2),
          tf.keras.layers.Dense(32, activation="relu"),
          tf.keras.layers.Dropout(0.2),
          tf.keras.layers.Dense(1, activation="tanh")
        ])
        self.technicality_model = tf.keras.models.Sequential([
          tf.keras.layers.Dense(128, activation="relu", input_shape=[len(OctaveBands)]),
          tf.keras.layers.Dropout(0.2),
          tf.keras.layers.Dense(64, activation="relu"),
          tf.keras.layers.Dropout(0.2),
          tf.keras.layers.Dense(32, activation="relu"),
          tf.keras.layers.Dropout(0.2),
          tf.keras.layers.Dense(1, activation="tanh")
        ])
        self.tonality_model_path = "models/tonality_dense.ckpt"
        self.technicality_model_path = "models/technicality_dense.ckpt"

class RecurrentNeuralNetworkModel(_TensorflowModelBase):
    """
    Predict tonality and technicality by recurrent neural network (lstm).
    To get best result:
    - Set OctaveBands to 1/6
    - Set UseRelativeSPL to True
    """

    def __init__(self):
        super().__init__()
        import tensorflow as tf
        self.tonality_model = tf.keras.models.Sequential([
          # Each timesteps has 1 feature
          tf.keras.layers.Reshape((-1, 1)),
          tf.keras.layers.LSTM(32),
          tf.keras.layers.Dropout(0.2),
          tf.keras.layers.Dense(1, activation="tanh")
        ])
        self.technicality_model = tf.keras.models.Sequential([
          tf.keras.layers.Reshape((-1, 1)),
          tf.keras.layers.LSTM(32),
          tf.keras.layers.Dropout(0.2),
          tf.keras.layers.Dense(1, activation="tanh")
        ])
        self.tonality_model_path = "models/tonality_lstm.ckpt"
        self.technicality_model_path = "models/technicality_lstm.ckpt"


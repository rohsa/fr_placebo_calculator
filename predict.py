import os
import sys
import numpy as np
import model

# Disable tensorflow messages
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

def main():
    if len(sys.argv) < 2:
        print(f"Please run: python3 {sys.argv[0]} \"Your Measurement L.txt\"")
        return -1
    paths = sys.argv[1:]
    xs = []
    for path in paths:
        if not os.path.isfile(path):
            print("file not exists:", path)
            continue
        fr = model.load_fr_as_data(path)
        if fr is None:
            print("load fr failed:", path)
            continue
        xs.append(fr)
    xs = np.array(xs)
    # m = model.DecisionTreeModel()
    m = model.SimpleNeuralNetworkModel()
    # m = model.RecurrentNeuralNetworkModel()
    ys = m.predict(xs)
    print(f"Model: {m.__class__.__name__} {getattr(m, 'Version', '1.0')}")
    for path, (tonality_score, technicality_score) in zip(paths, ys):
        print(os.path.splitext(os.path.basename(path))[0])
        print("  Tonality:", model.get_rank_name(tonality_score))
        print("  Technicality:", model.get_rank_name(technicality_score))

if __name__ == "__main__":
    exit(main())

